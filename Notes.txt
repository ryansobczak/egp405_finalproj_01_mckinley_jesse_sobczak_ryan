Temporeal delay.
Basically, client maintains a 100 millisecond lag behind the server.
This allows for a consistant state between clients.

Idea:
Broadcast packet:

int packet number;
float packet time;
5 slots, large (velocity, acceleration, pos)
12 slots, medium (total of 6 entities)
28 slots, small (total of 7 entities)

//theoretical, if 16 small units in the field, then only 4 medium, or 2 large can be in the scene.
//if 8 small, 4 medium, then only four large could be spawned.
//{1 1 1 1} 1
//2 2 2 2 {2} 2
//4 4 4 4 4 {4 4}
//alternative, 3, 8, 18
//1 1 1
//2 2 2 2
//4 4 4 4 4
2 slots, player data.
10 slots, player fire data. (may need to enlarge) is there a way to use variable size for packets?
//is this possible to replace with a time/bool pair? how many times per second can we fire?
slot for random number data.

