#pragma once

typedef int rType;
const int NUM_INDEX = 40;
struct RandomData
{
	rType Blocks[NUM_INDEX];
};

struct node
{
	rType data;
	node* next;
	void DestroyAll(){ if (next != nullptr){ next->DestroyAll(); delete next; next = nullptr; } }
};

class RandomDataStorage
{
public:
	RandomDataStorage();
	~RandomDataStorage();
	void Setup();
	rType GetNext();

	void Generate();
	
	void Save(RandomData);
	void Save(rType data[],int size);

	RandomData SendData();

private:
	int index;
	rType SavedData[NUM_INDEX];
	node* mFirst;
	node* mLast;
};
extern RandomDataStorage* gpRandom;