#include "LaserObject.h"



LaserObject::LaserObject()
{
	mTime = LIFE_TIME;
};
LaserObject::~LaserObject()
{
};
bool LaserObject::update(double deltaTime)
{
	mTime -= deltaTime;
	mTranform.RVel = mTime;
	if (mTime < 0)
		return true;

	mTranform.Pos.x = mTranform.Pos.x + mTranform.Vel.x;
	mTranform.Pos.y = mTranform.Pos.y + mTranform.Vel.y;

	mShape->setPosition(mTranform.Pos);
	BoarderCheck();
	return false;
};

void LaserObject::SetTransform(Transform trans)
{
	if (trans.RVel < mTime)
		mTime = trans.RVel;
	if (mTime == 0)
		mTime = LIFE_TIME;
	mTranform = trans;
	mTranform.RVel = mTime;
};