#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window.hpp>
#include <vector>
#include "PhysicsObjectManager.h"

class RenderGame 
{
public:
	RenderGame();
	~RenderGame();
	void initWindow(bool isCommandLine, sf::Texture ShipTexture);
	void SetPhysicsManager(PhysicsObjectManager* pManager);

	sf::Vector2i GetMousePosition();

	void Update(double deltaT);
	void Draw();

	inline sf::RenderWindow& getWindow(){ return mWindow; };

	inline int getScore(){ return mScoreNum; };
	inline int getLevel(){ return mLevelNum; };

	inline void setScore(int score){ mScoreNum = score; };
	inline void setLevel(int level){ mLevelNum = level; };

	void Player1Lives(int lives);
	void Player2Lives(int lives);
	int GetLivesPlayer1(){ return player1Lives.size(); }
	int GetLivesPlayer2(){ return player2Lives.size(); }
private:
	const std::string WINDOW_NAME = "Game Window";
	const std::string COMMAND_WINDOW_FONT = "Assets/framdit.ttf";
	const std::string NON_COMMAND_LINE_FONT = "../src/Assets/framdit.ttf";
	const float LIVES_SCALE = 0.05f;

	bool commandLine = false;

	sf::Shape* mBackground;
	sf::RenderWindow mWindow;
	sf::Event mEvent;

	PhysicsObjectManager* mpManager;

	std::vector<sf::Sprite> player1Lives;
	std::vector<sf::Sprite> player2Lives;

	sf::Font mpFont;
	sf::Text mScoreText;
	sf::Text mLevelText;
	sf::Text mP1Lives;
	sf::Text mP2Lives;
	sf::Texture mShipTexture;
	int mScoreNum;
	int mLevelNum;
};