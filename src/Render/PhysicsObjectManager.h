#pragma once
//Force commit by adding comment.
#include "PhysicsObject.h"
#include "..\render\CommonData.h"
#include <vector>
//include map

const float maxBoarder = (float)WINDOW_HEIGHT - 25.0f;
const float minBoarder = 25.0f;

typedef std::vector<PhysicsObject*> ObjectList;

struct LayerData
{
	bool Update;
	bool Draw;
	bool Destroy;
	int CollisionData;
	ObjectList Layer;
};
typedef std::map<int, LayerData*> LayerList;
typedef std::pair<int, LayerData*> LayerPair;


class PhysicsObjectManager
{
public:
	PhysicsObjectManager();
	~PhysicsObjectManager();

	void update(double deltaTime);
	void collide();
	void draw(sf::RenderWindow* window);

	void AddLayer(int Num, bool DoesUpdate, bool DoesDraw, bool DoesDestroy, int CollisionData);
	void AddToLayer(int LNum, PhysicsObject* Object);
	void RemoveFromLayer(int LNum, PhysicsObject* Object);
	void RemoveLayer(int LNum);
	void ClearLayer(int LNum);

	ObjectList* getLayer(int LayerNum);
private:
	int MaxLayer;
	LayerList mLayers;

};