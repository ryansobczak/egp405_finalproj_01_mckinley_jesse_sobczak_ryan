#include "RandomData.h"
#include <random>
using namespace std;
RandomDataStorage* gpRandom = nullptr;
RandomDataStorage::RandomDataStorage()
{
	mFirst = nullptr;
	mLast = nullptr;
	Setup();
};
RandomDataStorage::~RandomDataStorage()
{
	if (mFirst != nullptr)
	{
		mFirst->DestroyAll();
		delete mFirst;
		mFirst = nullptr;
	}
};
void RandomDataStorage::Setup()
{
	if (mFirst != nullptr)
	{
		mFirst->DestroyAll();
		delete mFirst;
	}
	index = 0;
	mFirst = new node;
	srand(0);
	mFirst->data = rand();
	mLast = mFirst;
	for (int i = 1; i < NUM_INDEX; i++)
	{
		mLast->next = new node;
		mLast = mLast->next;
		mLast->data = rand();
	}
	mLast->next = nullptr;
};
rType RandomDataStorage::GetNext()
{
	node* current = mFirst;
	if (mFirst->next == nullptr)
	{
		Generate();
	}
	rType data = mFirst->data;
	mFirst = mFirst->next;

	index++;
	delete current;
	printf("Random Num %i\n", data);
	return data;
};
void RandomDataStorage::Generate()
{
	while (index > 0)
	{
		mLast->next = new node;
		mLast = mLast->next;
		mLast->next = nullptr;

		mLast->data = rand();
		index--;
	}
}

void RandomDataStorage::Save(RandomData data)
{
	Save(data.Blocks, NUM_INDEX);
};
void RandomDataStorage::Save(rType data[], int size)
{
	if (mFirst != nullptr)
	{
		mFirst->DestroyAll();
		delete mFirst;
	}
	mFirst = nullptr;
	mLast = nullptr;
	node* current;

	current = mFirst = new node;
	current->data = data[0];

	for (int i = 1; i < size; i++)
	{
		current->next = new node;
		current = current->next;
		current->data = data[i];
	}
	current->next = nullptr;
	mLast = current;
	index = 0;
};

RandomData RandomDataStorage::SendData()
{
	if (index != 0)
	{
		Generate();
	}
	RandomData data;

	node* current = mFirst;
	for (int i = 0; i < NUM_INDEX; i++)
	{
		data.Blocks[i] = current->data;
		current = current->next;
	}
	return data;
};