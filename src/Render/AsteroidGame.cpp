#include "AsteroidGame.h"
#include "Astroid.h"
#include "LaserObject.h"
#include "RandomData.h"
#include "RakNetTime.h"
#include "GetTime.h"
#include <inttypes.h>

AstroidGame::AstroidGame(bool isCommandLine)
{
	SCREEN_WIDTH = WINDOW_WIDTH;
	SCREEN_HEIGHT = WINDOW_HEIGHT;

	CurrentTime = 0;

	if (gpRandom == nullptr)
		gpRandom = new RandomDataStorage;

	mMaxAstroid = 3;
	mCurrentLevel = 1;
	mISCommandLine = isCommandLine;
	if (isCommandLine)
	{
		if (!mShipTexture.loadFromFile("Assets/Ship.png"))
		{
			printf("Couldn't load ship sprite\n");
		}
		if (!mLazerTexture.loadFromFile("Assets/lazer.png"))
		{
			printf("Couldn't load lazer sprite\n");
		}
		if (!mAstroidTexture.loadFromFile("Assets/Astroids.png"))
		{
			printf("Couldn't load astroid spritesheet");
		}
	}
	else{
		if (!mShipTexture.loadFromFile("../Assets/Ship.png"))
		{
			printf("Couldn't load ship sprite\n");
		}
		if (!mLazerTexture.loadFromFile("../Assets/lazer.png"))
		{
			printf("Couldn't load lazer sprite\n");
		}
		if (!mAstroidTexture.loadFromFile("../Assets/Astroids.png"))
		{
			printf("Couldn't load astroid spritesheet");
		}
	}
	Astroid::SetSpriteSheet(&mAstroidTexture);
	Astroid::SetManager(&mManager);

	int astroidLayermask = (2 << 1) | (2 << 2) | (2 << 3);
	//test layer, layer 0 cannot collide.
	mManager.AddLayer(0, true, true, false, 0);
	//astroid layers
	mManager.AddLayer(1, true, true, false, 0);
	mManager.AddLayer(2, true, true, false, 0);
	mManager.AddLayer(3, true, true, false, 0);
	//player lazer layer
	mManager.AddLayer(4, true, true, true, astroidLayermask);
	mManager.AddLayer(5, true, true, true, astroidLayermask);
	//player layer
	mManager.AddLayer(6, true, true, false, astroidLayermask);

	mpShips[0] = new PlayerShip;
	CreateShip(mpShips[0]);
	mpShips[1] = new PlayerShip;
	CreateShip(mpShips[1]);
	mpShips[0]->SetPlayer(1);
	mpShips[1]->SetPlayer(2);
	mpPlayerShip = nullptr;

	mpWindow = nullptr;

	mDefaultLives[0] = 5;
	mDefaultLives[1] = 5;

	mGame.SetPhysicsManager(&mManager);
};
AstroidGame::~AstroidGame(){};

void AstroidGame::CreateScreen()
{
	mGame.initWindow(mISCommandLine, mShipTexture);
	mDefaultLives[0] = mLives[0] = mGame.GetLivesPlayer1();
	mDefaultLives[1] = mLives[1] = mGame.GetLivesPlayer2();
	mpWindow = &mGame.getWindow();
};
void AstroidGame::CreateShip(PhysicsObject* pShip)
{
	sf::Sprite* shipSprite = new sf::Sprite();
	Transform shipTrans;
	shipTrans.Pos = sf::Vector2f(WINDOW_WIDTH / 3, WINDOW_HEIGHT / 1.5);
	shipTrans.CurRotation = 0.0;
	shipTrans.Vel = sf::Vector2f(0, 0);
	shipTrans.RVel = 0.0;

	shipSprite->setTexture(mShipTexture);
	shipSprite->scale(sf::Vector2f(0.05f, 0.05f));
	shipSprite->setColor(sf::Color::White);

	pShip->SetTransform(shipTrans);
	pShip->setSprite(shipSprite);
};
void AstroidGame::ResetShip(PhysicsObject* pShip)
{
	Transform shipTrans;
	shipTrans.Pos = sf::Vector2f(WINDOW_WIDTH / 3, WINDOW_HEIGHT / 1.5);
	shipTrans.CurRotation = 0.0;
	shipTrans.Vel = sf::Vector2f(0, 0);
	shipTrans.RVel = 0.0;

	pShip->SetTransform(shipTrans);
};

void AstroidGame::CreateSandbox()
{
	mLives[0] = mDefaultLives[0];
	mLives[1] = mDefaultLives[1];
	mGame.Player1Lives(mLives[0]);
	mGame.Player2Lives(mLives[1]);
	for (int i = 0; i < 5; i++)
	{
		mManager.ClearLayer(i);
	}
	for (int i = 0; i < 2; i++)
	{
		mManager.RemoveFromLayer(6, mpShips[i]);
		ResetShip(mpShips[i]);
	}
	mManager.AddToLayer(6, mpShips[0]);
	mpPlayerShip = mpShips[0];

	if (gpRandom != nullptr)
	{
		delete gpRandom;
	}
	gpRandom = new RandomDataStorage;
	gpRandom->Setup();
	SpawnTimer = 0;
	mPlayerID = 1;
};
void AstroidGame::CreateBoard()
{
	CurrentTime = 0;
	mLives[0] = mDefaultLives[0];
	mLives[1] = mDefaultLives[1];
	mGame.Player1Lives(mLives[0]);
	mGame.Player2Lives(mLives[1]);
	mpPlayerShip = nullptr;
	for (int i = 0; i < 2; i++)
	{
		mManager.RemoveFromLayer(6, mpShips[i]);
		ResetShip(mpShips[i]);
		mManager.AddToLayer(6, mpShips[i]);
	}
	SpawnTimer = 0;

	for (int i = 0; i < 4; i++)
	{
		mManager.RemoveLayer(i);
		mManager.AddLayer(i, true, true, false, 0);
	}
};

void AstroidGame::SetPlayer(int PlayNum)
{
	mPlayerID = PlayNum;
	if (PlayNum <= 2)
	{
		mpPlayerShip = mpShips[PlayNum - 1];
	}
	else
	{
		printf("illegal player number\n");
	}
};
void AstroidGame::Update(double deltaT)
{
	deltaT *= 0.5f;
	CurrentTime += deltaT;
	if (gpRandom != nullptr)
	{
		SpawnTimer -= deltaT;
		if (SpawnTimer <= 0)
		{
			SpawnTimer += ((double)(gpRandom->GetNext() % 1000) / 100.0f);
			if (CheckSpawn())
			{
				Spawn();
			}
		}
	}
	if (mpWindow != nullptr)
	{
		Transform PlayerTrans;
		PlayerData data;
		sf::Event eEvent;
		while (mpWindow->pollEvent(eEvent))
		{
			switch (eEvent.type)
			{
			case sf::Event::Closed:
				mpWindow->close();
				break;
			case sf::Event::Resized:
				printf("Resized");
				break;
			case sf::Event::KeyPressed:
				if (mpPlayerShip != nullptr)
				switch (eEvent.key.code)
				{
				case sf::Keyboard::Escape:
					mpWindow->close();
					break;
				case sf::Keyboard::A:
					data.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					data.playerNum = mPlayerID;

					PlayerTrans = mpPlayerShip->GetTransform();

					PlayerTrans.RVel = -ROTATIONVEL;

					mpPlayerShip->SetTransform(PlayerTrans);
					data.playerTrans = PlayerTrans;

					//InputStack.push_back(data);
					break;

				case sf::Keyboard::D:
					data.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					data.playerNum = mPlayerID;

					PlayerTrans = mpPlayerShip->GetTransform();

					PlayerTrans.RVel = ROTATIONVEL;

					mpPlayerShip->SetTransform(PlayerTrans);
					data.playerTrans = PlayerTrans;

					//InputStack.push_back(data);
					break;
				case sf::Keyboard::S:
					mpPlayerShip->Deccelerate(VEL_ACC*deltaT);
					break;
				case sf::Keyboard::W:
					mpPlayerShip->Accelerate(VEL_ACC*deltaT);
					break;
				case sf::Keyboard::Space:
				{
					printf("shoot\n");
					data.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					data.playerNum = 0;

					Transform bulletTrans;
					bulletTrans = mpPlayerShip->GetTransform();

					FireLaser(bulletTrans);

					data.playerTrans = bulletTrans;

					//InputStack.push_back(data);
					break;
				}
				default:
					break;
				}
				break;
			case sf::Event::KeyReleased:
			{
				if (mpPlayerShip != nullptr)
				switch (eEvent.key.code)
				{
				case sf::Keyboard::A:
					data.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					data.playerNum = mPlayerID;

					PlayerTrans = mpPlayerShip->GetTransform();

					PlayerTrans.RVel = 0;

					mpPlayerShip->SetTransform(PlayerTrans);
					data.playerTrans = PlayerTrans;

					//InputStack.push_back(data);
					break;

				case sf::Keyboard::D:
					data.packetID = (unsigned char)messages::ID_TTT_PLAYER_MOVED;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					data.playerNum = mPlayerID;

					PlayerTrans = mpPlayerShip->GetTransform();

					PlayerTrans.RVel = 0;

					mpPlayerShip->SetTransform(PlayerTrans);
					data.playerTrans = PlayerTrans;

					//InputStack.push_back(data);
					break;
				case sf::Keyboard::P:
				{
					printf("change level\n");

					if (ASTROID_LEVEL_SCALE_MULTIPLE >= 2.5f)
					{
						ASTROID_LEVEL_SCALE_MULTIPLE += 0.1f;
					}
					if (mMaxAstroid < 10)
					{
						if (mCurrentLevel % 2 == 0)
						{
							mMaxAstroid++;
						}
					}
					mCurrentLevel++;
					mGame.setLevel(mCurrentLevel);

					data.packetID = (unsigned char)messages::ID_TTT_CHANGE_LEVEL;
					//data.useTimeStamp = ID_TIMESTAMP;
					data.timeStamp = CurrentTime;
					//InputStack.push_back(data);
					break;
				}
				default:
					break;
				}
			}
			break;
			default:
				break;
			}
		}
	}
	if (mpShips[0] != nullptr)
	{
		if (!mpShips[0]->isLive())
		{
			mLives[0]--;
			mGame.Player1Lives(mLives[0]);
			if (mLives[0] <= 0)
			{
				mManager.RemoveFromLayer(6, mpShips[0]);
				if (mPlayerID == 1)
				{
					mpPlayerShip = nullptr;
				}
			}
		}
	}
	if (mpShips[1] != nullptr)
	{
		if (!mpShips[1]->isLive())
		{
			mLives[1]--;
			mGame.Player2Lives(mLives[1]);
			if (mLives[1] <= 0)
			{
				mManager.RemoveFromLayer(6, mpShips[1]);
				if (mPlayerID == 2)
				{
					mpPlayerShip = nullptr;
				}
			}
		}
	}
	mGame.Update(deltaT);
	mGame.setScore(POINTS_TOTAL);
};
void AstroidGame::Draw()
{
	mGame.Draw();
};

bool AstroidGame::InputStackCheck()
{
	return InputStack.size();
};

ServerData AstroidGame::GetServerState()
{
	ServerData data;
	data.packetID = messages::ID_TTT_STATE_BROADCAST;
	data.timeStamp = CurrentTime; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value

	data.Player1 = mpShips[0]->GetTransform();
	data.Player2 = mpShips[1]->GetTransform();
	data.score = POINTS_TOTAL;
	data.RandomNum = gpRandom->SendData();

	ObjectList PhysicsLayer = *mManager.getLayer(3);
	int size = PhysicsLayer.size();
	int i = 0;
	for (i = 0; i < size; i++)
	{
		data.Astroids[i] = PhysicsLayer[i]->GetTransform();
	}
	data.HighMed = i;
	PhysicsLayer = *mManager.getLayer(2);
	size = PhysicsLayer.size();
	for (int j = 0; j < size; j++)
	{
		data.Astroids[i] = PhysicsLayer[j]->GetTransform();
		i++;
	}
	data.MedLow = i;
	PhysicsLayer = *mManager.getLayer(1);
	size = PhysicsLayer.size();
	for (int j = 0; j < size; j++)
	{
		data.Astroids[i] = PhysicsLayer[j]->GetTransform();
		i++;
	}
	data.low = i;

	PhysicsLayer = *mManager.getLayer(4);
	data.Num1 = PhysicsLayer.size();
	for (int j = 0; j < data.Num1; j++)
	{
		data.Laser1[j] = PhysicsLayer[j]->GetTransform();
	}
	PhysicsLayer = *mManager.getLayer(5);
	data.Num2 = PhysicsLayer.size();
	for (int j = 0; j < data.Num2; j++)
	{
		data.Laser2[j] = PhysicsLayer[j]->GetTransform();
	}

	data.score = POINTS_TOTAL;
	data.p1Lives = mLives[0];
	data.p2Lives = mLives[1];
	data.level = mCurrentLevel;

	return data;
};
void AstroidGame::SetServerState(ServerData data)
{
	//data.timeStamp 
	if (mPlayerID == 1)
	{
		Transform current = mpShips[1]->GetTransform();
		Transform Dest = data.Player2;
		Transform Result;
		Result.Pos = current.Pos;
		Result.RVel = 0;
		Result.CurRotation = Dest.CurRotation;
		float deltaX = Dest.Pos.x - current.Pos.x;
		float deltaY = Dest.Pos.y - current.Pos.y;
		float tempX;
		float tempY;
		if(deltaY > SCREEN_HEIGHT / 2)
		{
			tempY = Dest.Pos.y - SCREEN_HEIGHT;
			deltaY = tempY - current.Pos.y;
		}
		else if (deltaY < -SCREEN_HEIGHT / 2)
		{
			tempY = Dest.Pos.y + SCREEN_HEIGHT;
			deltaY = tempY - current.Pos.y;
		}
		if (deltaX > SCREEN_WIDTH / 2)
		{
			tempX = Dest.Pos.x - SCREEN_WIDTH;
			deltaX = tempX - current.Pos.x;
		}
		else if (deltaX < -SCREEN_WIDTH / 2)
		{
			tempX = Dest.Pos.x + SCREEN_WIDTH;
			deltaX = tempX - current.Pos.x;
		}
		Result.Vel.y = (deltaY) / (data.timeStamp - CurrentTime);
		Result.Vel.x = (deltaX) / (data.timeStamp - CurrentTime);
		mpShips[1]->SetTransform(Result);
	}
	else
	{
		Transform current = mpShips[0]->GetTransform();
		Transform Dest = data.Player1;
		Transform Result;
		Result.Pos = current.Pos;
		Result.RVel = 0;
		Result.CurRotation = Dest.CurRotation;
		float deltaX = Dest.Pos.x - current.Pos.x;
		float deltaY = Dest.Pos.y - current.Pos.y;
		float tempX;
		float tempY;
		if (deltaY > SCREEN_HEIGHT / 2)
		{
			tempY = Dest.Pos.y - SCREEN_HEIGHT;
			deltaY = tempY - current.Pos.y;
		}
		else if (deltaY < -SCREEN_HEIGHT / 2)
		{
			tempY = Dest.Pos.y + SCREEN_HEIGHT;
			deltaY = tempY - current.Pos.y;
		}
		if (deltaX > SCREEN_WIDTH / 2)
		{
			tempX = Dest.Pos.x - SCREEN_WIDTH;
			deltaX = tempX - current.Pos.x;
		}
		else if (deltaX < -SCREEN_WIDTH / 2)
		{
			tempX = Dest.Pos.x + SCREEN_WIDTH;
			deltaX = tempX - current.Pos.x;
		}
		Result.Vel.y = (deltaY) / (data.timeStamp - CurrentTime);
		Result.Vel.x = (deltaX) / (data.timeStamp - CurrentTime);
		mpShips[0]->SetTransform(Result);
	}

	//is gprandom null?


	ObjectList* PhysicsLayer = mManager.getLayer(3);
	int size = PhysicsLayer->size();
	int i = 0;
	int k = 0;
	int minimum = data.HighMed;
	if (size > minimum)
	{
		for (int j = size; j > minimum; j--)
		{
			delete (*PhysicsLayer)[j - 1];
			PhysicsLayer->pop_back();
		}
		size = PhysicsLayer->size();
	}
	for (i = 0; i < size && i < data.HighMed; i++)
	{
		(*PhysicsLayer)[i]->SetTransform(data.Astroids[i]);
	}
	for (int j = size; j < data.HighMed; j++)
	{
		Astroid* temp = new Astroid;
		temp->Setup(3);
		temp->SetTransform(data.Astroids[j]);
		PhysicsLayer->push_back(temp);
	}

	PhysicsLayer = mManager.getLayer(2);
	size = PhysicsLayer->size();
	minimum = data.MedLow - data.HighMed;
	if (size > minimum)
	{
		for (int j = size; j > minimum; j--)
		{
			delete (*PhysicsLayer)[j - 1];
			PhysicsLayer->pop_back();
		}
		size = PhysicsLayer->size();
	}
	for (int j = 0; j < size && i < data.MedLow; j++)
	{
		(*PhysicsLayer)[j]->SetTransform(data.Astroids[i]);
		i++;
	}
	for (int j = size; j < data.MedLow; j++)
	{
		Astroid* temp = new Astroid;
		temp->Setup(2);
		temp->SetTransform(data.Astroids[j]);
		PhysicsLayer->push_back(temp);
	}

	PhysicsLayer = mManager.getLayer(1);
	size = PhysicsLayer->size();
	minimum = data.low - data.MedLow;
	if (size > minimum)
	{
		for (int j = size; j > minimum; j--)
		{
			delete (*PhysicsLayer)[j - 1];
			PhysicsLayer->pop_back();
		}
		size = PhysicsLayer->size();
	}
	for (int j = 0; j < size && i < data.low; j++)
	{
		(*PhysicsLayer)[j]->SetTransform(data.Astroids[i]);
		i++;
	}
	for (int j = size; j < data.low; j++)
	{
		Astroid* temp = new Astroid;
		temp->Setup(1);
		temp->SetTransform(data.Astroids[j]);
		PhysicsLayer->push_back(temp);
	}

	Transform temp;
	PhysicsLayer = mManager.getLayer(4);
	size = PhysicsLayer->size();
	for (int j = 0; j < data.Num1 && j < size; j++)
	{
		(*PhysicsLayer)[j]->SetTransform(data.Laser1[j]);
	}
	for (int j = size; j < data.Num1; j++)
	{
		FireLaser(data.Laser1[j], 1);
		temp = (*PhysicsLayer)[j]->GetTransform();
		temp.RVel = data.Laser1[j].RVel;
		(*PhysicsLayer)[j]->SetTransform(data.Laser1[j]);
	}

	PhysicsLayer = mManager.getLayer(5);
	size = PhysicsLayer->size();
	for (int j = 0; j < data.Num2 && j < size; j++)
	{
		(*PhysicsLayer)[j]->SetTransform(data.Laser2[j]);
	}
	for (int j = size; j < data.Num2; j++)
	{
		FireLaser(data.Laser2[j], 2);
		temp = (*PhysicsLayer)[j]->GetTransform();
		temp.RVel = data.Laser2[j].RVel;
		(*PhysicsLayer)[j]->SetTransform(data.Laser2[j]);
	}

	mLives[0] = data.p1Lives;
	mLives[1] = data.p2Lives;

	if (data.level != mCurrentLevel)
	{
		SetLevel(data.level);
	}

	gpRandom->Save(data.RandomNum);
	POINTS_TOTAL = data.score;
};

ClientData AstroidGame::GetClientState()
{
	ClientData data;
	ObjectList lasers;

	data.timeStamp = CurrentTime;
	data.packetID = messages::ID_TTT_PLAYER_UPDATE;
	data.PlayerNum = mPlayerID;
	if (mpPlayerShip != nullptr)
	{
		data.Player = mpPlayerShip->GetTransform();
	}
	if (mPlayerID == 1)
	{
		lasers = *mManager.getLayer(4);
	}
	else
	{
		lasers = *mManager.getLayer(5);
	}
	data.num = lasers.size();
	for (int i = 0; i < data.num && i < 20; i++)
	{
 		data.Laser[i] = lasers[i]->GetTransform();
	}

	data.level = mCurrentLevel;
	return data;
};
void AstroidGame::SetClientState(ClientData data)
{
	ObjectList lasers;
	if (data.PlayerNum == 1)
	{
		mpShips[0]->SetTransform(data.Player);
		mpShips[0]->update(CurrentTime - data.timeStamp);
		lasers = *mManager.getLayer(4);
	}
	else
	{
		mpShips[1]->SetTransform(data.Player);
		mpShips[1]->update(CurrentTime - data.timeStamp);
		lasers = *mManager.getLayer(5);
	}
	int size = lasers.size();
	for (int i = 0; i < data.num && i < size; i++)
	{
		lasers[i]->SetTransform(data.Laser[i]);
	}
	for (int i = size; i < data.num; i++)
	{
		FireLaser(data.Laser[i], data.PlayerNum);
	}
	
	if (mCurrentLevel < data.level)
	{
		mCurrentLevel = data.level;
	}

};

void AstroidGame::FireLaser(Transform lazerTrans)
{
	FireLaser(lazerTrans, mPlayerID);
}
void AstroidGame::FireLaser(Transform lazerTrans, int player)
{
	sf::Sprite* pLazerS = new sf::Sprite();
	pLazerS->setTexture(mLazerTexture);
	pLazerS->scale(sf::Vector2f(0.09f, 0.09f));
	pLazerS->setRotation(lazerTrans.CurRotation);

	PhysicsObject* plazerPO = new LaserObject();
	lazerTrans.RVel = 0;

	float xVel = sin(lazerTrans.CurRotation * PI / 180.0)*VEL_LAZER;
	float yVel = -cos(lazerTrans.CurRotation * PI / 180.0)*VEL_LAZER;
	lazerTrans.Vel = sf::Vector2f(xVel, yVel);
	plazerPO->SetTransform(lazerTrans);
	plazerPO->setSprite(pLazerS);

	if (player == 1)
	{
		mManager.AddToLayer(4, plazerPO);
	}
	else
	{
		mManager.AddToLayer(5, plazerPO);
	}
};


bool AstroidGame::CheckSpawn()
{
	int CurrentAstroids;
	int Large;
	int Medium;
	int Small;
	int diff;
	ObjectList* list = mManager.getLayer(1);
	Small = list->size();
	list = mManager.getLayer(2);
	Medium = list->size();
	list = mManager.getLayer(3);
	Large = list->size();
	diff = Medium % 2;
	Medium /= 2;
	Medium += diff > 0 ? 1 : 0;
	if (Medium > 0)
	{
		if (Medium >= SPARE_ASTROID)
			Medium -= SPARE_ASTROID;
		else
			Medium = 0;
	}
	diff = Large % 4;
	Small /= 4;
	Small += diff > 0 ? 1 : 0;
	if (Small > 0)
	{
		if (Small >= SPARE_ASTROID)
			Small -= SPARE_ASTROID;
		else
			Small = 0;
	}
	CurrentAstroids = Small + Medium + Large;

	if (CurrentAstroids < mMaxAstroid)
	{
		return true;
	}

	return false;
};
void AstroidGame::Spawn()
{
	printf("Create Astroid\n");
	Astroid* temp = new Astroid();
	temp->CreateBase(3);
	mManager.AddToLayer(3, temp);
	Transform ATrans = temp->GetTransform();
	
	int xPos = gpRandom->GetNext() % WINDOW_WIDTH;
	int yPos = gpRandom->GetNext() % WINDOW_HEIGHT;

	ATrans.Pos = sf::Vector2f(xPos, yPos);
	temp->SetTransform(ATrans);
};


void AstroidGame::SetLevel(int level)
{
	ASTROID_LEVEL_SCALE_MULTIPLE = 1 + ((level-1)*0.1f);
	if (ASTROID_LEVEL_SCALE_MULTIPLE >= 2.5f)
	{
		ASTROID_LEVEL_SCALE_MULTIPLE = 2.5f;
	}

	mMaxAstroid = mCurrentLevel / 2 - mCurrentLevel % 2 + 1;
	if (mMaxAstroid > 10)
	{
		mMaxAstroid = 10;
	}

	mCurrentLevel = level;
	mGame.setLevel(mCurrentLevel);
};