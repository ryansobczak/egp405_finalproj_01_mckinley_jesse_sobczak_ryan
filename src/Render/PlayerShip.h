#pragma once

#include "PhysicsObject.h"

class PlayerShip : public PhysicsObject
{
	const float MAX_SPEED = 0.3f;
	const float INV_TIME = 1000.0f;
public:
	PlayerShip();
	~PlayerShip();

	virtual bool update(double deltaTime);
	//virtual void draw(sf::RenderWindow* window);
	virtual void SetTransform(Transform trans);


	virtual bool CheckCollision(PhysicsObject* obj);
	virtual void Collision(PhysicsObject* obj);

	void Accelerate(float inc);
	void Deccelerate(float inc);


	void SetPlayer(int PlayerNum);

	float GetCurVel(){ return curVel; };
	bool isLive();
protected:
	Transform mPrevTransform;
	float curVel;

	float invTime;
	bool invincible;
	bool live;
};