#pragma once
#include "renderGame.h"
#include "CommonData.h"
#include "PlayerShip.h"

class AstroidGame
{
public:
	const int PLAYER_LAYER = 6;

	const float ROTATIONVEL = 0.25f;
	const float VEL_ACC = 0.2f;
	const float VEL_LAZER = 15.0f;

	AstroidGame(bool isCommandLine);
	~AstroidGame();

	void CreateScreen();
	void CreateSandbox();
	void CreateBoard();

	void SetPlayer(int PlayNum);

	void Update(double deltaT);
	void Draw();

	bool InputStackCheck();

	ServerData GetServerState();
	void SetServerState(ServerData data);

	ClientData GetClientState();
	void SetClientState(ClientData data);

	void SetLevel(int level);
	inline int GetLevel(){ return mGame.getLevel(); };

	void SetTime(double time){ CurrentTime = time; }

private:
	void CreateShip(PhysicsObject* pShip);
	void ResetShip(PhysicsObject* pShip);
	void FireLaser(Transform lazerTrans);
	void FireLaser(Transform lazerTrans,int player);

	bool CheckSpawn();
	void Spawn();
	PlayerShip* mpShips[2];
	int mLives[2];
	int mDefaultLives[2];
	

	PlayerShip* mpPlayerShip;

	PhysicsObjectManager mManager;
	RenderGame mGame;
	sf::RenderWindow* mpWindow;

	sf::Texture mShipTexture;
	sf::Texture mLazerTexture;
	sf::Texture mAstroidTexture;

	bool mISCommandLine;

	std::vector<PlayerData> InputStack;
	double SpawnTimer;
	double CurrentTime;
	int mPlayerID;


	int mCurrentLevel;
	int mMaxAstroid;

	std::vector<ServerData> ServerDataStorage;
	
};