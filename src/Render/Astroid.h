#pragma once
#include "PhysicsObject.h"

class PhysicsObjectManager;
class Astroid : public PhysicsObject
{
public:
	const float BASE_SCALE = 0.1f;

	const int TEXTURE_WIDTH = 1200;
	const int TEXTURE_HEIGHT = 1200;
	const int TEXTURE_ROWS = 4;
	const int TEXTURE_COLS = 4;

	const int ROTATION_MAX = 5;
	const int ROTATION_MIN = 2;
	const int ROTATION_VARIANCE = ROTATION_MAX - ROTATION_MIN;


	const float ROTATION_REDUCTION = 0.04f;

	const int MAX_VEL = 5;
	const int MIN_VEL = 1;
	const int VAR_VEL = MAX_VEL - MIN_VEL;
	const float RED_VEL = 0.02f;

	const int MAX_LAYERS = 3;
	Astroid();
	~Astroid();

	void Create(int layer, float xPos, float yPos);
	void CreateBase(int layer);

	void Setup(int layer);

	virtual bool update(double deltaTime);

	virtual bool CheckCollision(PhysicsObject* obj);
	virtual void Collision(PhysicsObject* obj);

	virtual void draw(sf::RenderWindow* window);
	
	static void SetManager(PhysicsObjectManager* manager);
	static void SetSpriteSheet(sf::Texture* SpriteSheet);


	virtual void SetTransform(Transform trans);
protected:
	int mLayer;

	void CreateSub(int layer);

	sf::Sprite* mpSprite;

	bool collision;

	static sf::Texture* spSpriteSheet;
	static PhysicsObjectManager* spManager;
	static int sCount;
};

extern float ASTROID_LEVEL_SCALE_MULTIPLE;
extern int POINTS_TOTAL;