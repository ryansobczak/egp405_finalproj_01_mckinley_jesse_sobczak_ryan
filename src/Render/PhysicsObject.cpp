#include "PhysicsObject.h"
#include "Astroid.h"
#include "PlayerShip.h"

#include <random>
#include <math.h>
using namespace std;
int SCREEN_WIDTH = 0;
int SCREEN_HEIGHT = 0;
//Force commit by adding comment.
PhysicsObject::PhysicsObject()
{
	mShape = nullptr;
	mTranform.CurRotation = 0;
	mTranform.Vel = sf::Vector2f(0,0);
	mTranform.RVel = 0.0;
	mTranform.Pos = sf::Vector2f(0, 0);
};
PhysicsObject::~PhysicsObject()
{
	if (mShape != nullptr)
	{
		delete mShape;
		mShape = nullptr;
	}
};

bool PhysicsObject::update(double deltaTime)
{
	mTranform.Pos.x = mTranform.Pos.x + mTranform.Vel.x;
	mTranform.Pos.y= mTranform.Pos.y + mTranform.Vel.y;

	mShape->setPosition(mTranform.Pos);
	BoarderCheck();
	return false;
};

void PhysicsObject::draw(sf::RenderWindow* window)
{
	if (mShape != nullptr)
		window->draw(*mShape);
};

bool PhysicsObject::CheckCollision(PhysicsObject* obj)
{
	sf::Vector2f diff = obj->GetTransform().Pos - mTranform.Pos;
	float distance = sqrt(diff.x*diff.x + diff.y*diff.y);
	float radius = mRadius + obj->mRadius;
	if (distance < radius)
	{
		return true;
	}
	return false;
};

void PhysicsObject::Collision(PhysicsObject* obj){};

Transform PhysicsObject::GetTransform()
{
	return mTranform;
};
void PhysicsObject::setSprite(sf::Sprite* sprite)
{
	mShape = sprite;
	
	sf::IntRect shape = mShape->getTextureRect();
	mShape->setOrigin(shape.width/2,shape.height/2);
	mShape->setPosition(mTranform.Pos);

	sf::Vector2f scale = mShape->getScale();
	mRadius = (shape.width*scale.x + shape.height*scale.y) / 4;
};

void PhysicsObject::SetTransform(Transform trans)
{
	mTranform = trans;
};

void PhysicsObject::BoarderCheck()
{
	if (mTranform.Pos.x < -mRadius)
	{
		mTranform.Pos.x = (SCREEN_WIDTH + mRadius);
	}
	else if (mTranform.Pos.x > (SCREEN_WIDTH + mRadius))
	{
		mTranform.Pos.x = -mRadius;
	}
	if (mTranform.Pos.y < -mRadius)
	{
		mTranform.Pos.y = (SCREEN_HEIGHT + mRadius);
	}
	else if (mTranform.Pos.y >(SCREEN_HEIGHT + mRadius))
	{
		mTranform.Pos.y = -mRadius;
	}
};
//CircleCollider::CircleCollider(float radius, sf::Shape* object) : CollisionData(object)
//{
//	mRadius = radius;
//};
//bool CircleCollider::GetCollision(sf::Shape* obj)
//{
//	sf::FloatRect tempCheck = obj->getGlobalBounds();
//	sf::FloatRect base = BaseObject->getGlobalBounds();
//	return (tempCheck.intersects(base));
//};
//RectCollider::RectCollider(sf::Shape* object) : CollisionData(object)
//{
//};
//bool RectCollider::GetCollision(sf::Shape* obj)
//{
//	sf::FloatRect tempCheck = obj->getGlobalBounds();
//	sf::FloatRect base = BaseObject->getGlobalBounds();
//	return (tempCheck.intersects(base));
//};
