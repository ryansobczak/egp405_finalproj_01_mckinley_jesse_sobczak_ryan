#include "PhysicsObjectManager.h"

//Force commit by adding comment.
PhysicsObjectManager::PhysicsObjectManager(){ MaxLayer = 0; };
PhysicsObjectManager::~PhysicsObjectManager()
{

	LayerData *Data;
	PhysicsObject* Object;
	for (LayerList::iterator Layer = mLayers.begin(); Layer != mLayers.end(); Layer++)
	{
		Data = Layer->second;
		int size = Data->Layer.size();
		for (int i = 0; i < size; i++)
		{
			delete (Data->Layer)[i];
		}
		Data->Layer.clear();
		delete Data;
	}
	mLayers.clear();
};

void PhysicsObjectManager::update(double deltaTime)
{
	LayerData *Data;
	PhysicsObject* Object;
	for (LayerList::iterator Layer = mLayers.begin(); Layer != mLayers.end(); Layer++)
	{
		Data = Layer->second;
		if (Data->Update)
		{
			int size = Data->Layer.size();
			for (int i = 0; i < size; i++)
			{
				Object = (Data->Layer)[i];
				if (Object->update(deltaTime))
				{
					Data->Layer.erase(Data->Layer.begin() + i);
					delete Object;
					size--; i--;
				}
			}
		}
	}
	collide();
};
void PhysicsObjectManager::collide()
{
	LayerData *Data;
	LayerData *Collision;
	PhysicsObject* Object;
	PhysicsObject* ObjectC;
	
	bool Remove;
	bool ShouldRemove;
	bool RemoveC;
	for (LayerList::iterator Layer = mLayers.begin(); Layer != mLayers.end(); Layer++)
	{
		Data = Layer->second;
		if (Data->CollisionData != 0)
		{
			for (LayerList::iterator CLayer = mLayers.begin(); CLayer != mLayers.end(); CLayer++)
			{
				RemoveC = CLayer->second->Destroy;
				int LayerNum = CLayer->first;
				if (Data->CollisionData & (2 << (LayerNum)))
				{
					Collision = CLayer->second;
					int size = Data->Layer.size();
					int sizeC = Collision->Layer.size();
					for (int i = 0; i < size; i++)
					{
						Remove = false;
						Object = (Data->Layer)[i];
						for (int k = 0; k < sizeC; k++)
						{
							ObjectC = (Collision->Layer)[k];

							if (Object->CheckCollision(ObjectC))
							{
 								Object->Collision(ObjectC);
								ObjectC->Collision(Object);
								Remove = true;
								if (RemoveC)
								{
									(Collision->Layer).erase(Collision->Layer.begin() + k);
									k--;
									sizeC--;
									delete ObjectC;
								}
							}
						}
						if (Remove && Data->Destroy)
						{
							Data->Layer.erase(Data->Layer.begin() + i);
							i--;
							size--;
							delete Object;
						}
					}
				}
			}
		}
	}
};

void PhysicsObjectManager::draw(sf::RenderWindow* window)
{

	LayerData *Data;
	PhysicsObject* Object;
	for (LayerList::iterator Layer = mLayers.begin(); Layer != mLayers.end(); Layer++)
	{
		Data = Layer->second;
		if (Data->Draw)
		{
			int size = Data->Layer.size();
			for (int i = 0; i < size; i++)
			{
				Object = (Data->Layer)[i];
				Object->draw(window);
			}
		}
	}
};

void PhysicsObjectManager::AddLayer(int Num, bool DoesUpdate, bool DoesDraw, bool DoesDestroy, int CollisionData)
{
	if (mLayers.find(Num) == mLayers.end())
	{
		LayerPair newLayer(Num, new LayerData);
		LayerData *Data = newLayer.second;
		Data->Update = DoesUpdate;
		Data->Draw = DoesDraw;
		Data->Destroy = DoesDestroy;
		Data->CollisionData = CollisionData;
		mLayers.insert(newLayer);

		if (MaxLayer < Num)
		{
			MaxLayer = Num;
		}
	}
};
void PhysicsObjectManager::AddToLayer(int LNum, PhysicsObject* Object)
{
	//can't add the object to the layer
	if (mLayers.find(LNum) != mLayers.end())
	{
		LayerData *Data = mLayers[LNum];

		Data->Layer.push_back(Object);
	}
};
void PhysicsObjectManager::RemoveFromLayer(int LNum, PhysicsObject* Object)
{
	printf("Remove Layer\n");
	if (mLayers.find(LNum) != mLayers.end())
	{
		LayerData *Data = mLayers[LNum];

		int size = Data->Layer.size();
		for (int i = 0; i < size; i++)
		{ 
			if (Data->Layer[i] == Object)
			{
				Data->Layer.erase(Data->Layer.begin() + i);
				i = size;
			}
		}
	}
};


void PhysicsObjectManager::RemoveLayer(int LNum)
{
	if (mLayers.find(LNum) != mLayers.end())
	{
		LayerData *Data = mLayers[LNum];
		int size = Data->Layer.size();
		for (int i = 0; i < size; i++)
		{
			delete Data->Layer[i];
		}
		Data->Layer.clear();
		delete Data;
		mLayers.erase(LNum);
	}
};
void PhysicsObjectManager::ClearLayer(int LNum)
{
	if (mLayers.find(LNum) != mLayers.end())
	{
		LayerData *Data = mLayers[LNum];
		int size = Data->Layer.size();
		for (int i = 0; i < size; i++)
		{
			delete (Data->Layer)[i];
		}
		Data->Layer.clear();
	}
};

ObjectList* PhysicsObjectManager::getLayer(int LayerNum)
{
	if (mLayers.find(LayerNum) != mLayers.end())
	{
		return &(mLayers[LayerNum]->Layer);
	}
	return nullptr;
};