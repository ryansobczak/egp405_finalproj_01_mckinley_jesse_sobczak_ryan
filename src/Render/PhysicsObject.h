#pragma once

//Force commit by adding comment.
#include <SFML\Graphics.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window.hpp>
#include "..\render\CommonData.h"


class PhysicsObject
{
public:
	const int RAND_VARIANCE = 5;
	const float RAND_MULT = 0.1f;
	PhysicsObject();
	~PhysicsObject();

	//return true if it needs to be deleted.
	virtual bool update(double deltaTime);

	virtual bool CheckCollision(PhysicsObject* obj);
	virtual void Collision(PhysicsObject* obj);
	
	virtual void draw(sf::RenderWindow* window);

	Transform GetTransform();
	virtual void SetTransform(Transform trans);

	void setSprite(sf::Sprite* sprite);

	inline float GetRadius(){ return mRadius; };

	virtual void BoarderCheck();

protected :
	//void calculateNewVelocity();

	Transform mTranform;

	sf::Sprite* mShape;

	float mRadius;
};
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;