#pragma once
#include "PhysicsObject.h"

class LaserObject : public PhysicsObject
{
	const float LIFE_TIME = 250.0f;
public:

	LaserObject();
	~LaserObject();
	virtual bool update(double deltaTime);

	virtual void SetTransform(Transform trans);

protected:
	float mTime;
};