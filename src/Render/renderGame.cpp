#include "renderGame.h"
#include "SFML\System\Vector2.hpp"
#include "..\render\CommonData.h"
#include "..\Render\Astroid.h"
#include "..\Render\PlayerShip.h"

#include<iostream>
#include<SFML/System/String.hpp>
#include <SFML/Window/Keyboard.hpp>

RenderGame::RenderGame()
{}

RenderGame::~RenderGame() {}

void RenderGame::initWindow(bool isCommandLine, sf::Texture ShipTexture)
{
	commandLine = isCommandLine;
	mShipTexture = ShipTexture;
	mLevelNum = 1;

	mWindow.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);
	//mWindow->setFramerateLimit(60);
	
	if (!commandLine)
	{
		mpFont.loadFromFile(NON_COMMAND_LINE_FONT);
	}
	if (commandLine)
	{
		mpFont.loadFromFile(COMMAND_WINDOW_FONT);
	}

	mScoreText.setFont(mpFont); 
	mScoreText.setString("Score: 0");
	mScoreText.setCharacterSize(18);
	mScoreText.setPosition(10, 10);
	mScoreText.setColor(sf::Color::White);

	mLevelText.setFont(mpFont);
	mLevelText.setString("Level: 1");
	mLevelText.setCharacterSize(18);
	mLevelText.setPosition(200, 10);
	mLevelText.setColor(sf::Color::White);

	mP1Lives.setFont(mpFont); 
	mP1Lives.setString("Player1 Lives");
	mP1Lives.setCharacterSize(12);
	mP1Lives.setPosition(5, WINDOW_HEIGHT - 55);
	mP1Lives.setColor(sf::Color::White);

	mP2Lives.setFont(mpFont);
	mP2Lives.setString("Player2 Lives");
	mP2Lives.setCharacterSize(12);
	mP2Lives.setPosition(WINDOW_WIDTH - mP2Lives.getGlobalBounds().width - 5, WINDOW_HEIGHT - 55);
	mP2Lives.setColor(sf::Color::White);

	float xPos;
	float xScale = 1.1*LIVES_SCALE*mShipTexture.getSize().x;
	float yPos;
	float yScale = 1.01*(LIVES_SCALE * mShipTexture.getSize().y);

	for (int i = 0; i < STARTING_LIVES; i++)//P1
	{
		sf::Sprite sprite;
		sprite.setTexture(mShipTexture);
		sprite.scale(sf::Vector2f(LIVES_SCALE, LIVES_SCALE));
		sprite.setColor(sf::Color::White);
		xPos = (float)5 + (i * xScale);
		yPos = (float)WINDOW_HEIGHT - 5 - yScale;
		sprite.setPosition(xPos, yPos);
		//printf("%f\n", sprite.getPosition().x);
		player1Lives.push_back(sprite);	
	}
	for (int j = 0; j < STARTING_LIVES; j++)//P2
	{
		sf::Sprite sprite;
		sprite.setTexture(mShipTexture);
		sprite.scale(sf::Vector2f(0.05f, 0.05f));
		sprite.setColor(sf::Color::White);
		xPos = (float)WINDOW_WIDTH - 20 - (j * xScale);
		yPos = (float)WINDOW_HEIGHT - 5 - yScale;
		sprite.setPosition(xPos, yPos);
		//printf("%f\n", sprite.getPosition().x);
		player2Lives.push_back(sprite);
	}

	mBackground = new sf::RectangleShape(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));
	mBackground->setPosition(sf::Vector2f(0, 0));
	mBackground->setFillColor(sf::Color::Black);
}

void RenderGame::SetPhysicsManager(PhysicsObjectManager* pManager)
{
	mpManager = pManager;
};
void RenderGame::Update(double deltaT)
{
	//sf::Event eEvent;
	//while (mWindow.pollEvent(eEvent))
	//{
	//	// Window closed
	//	if (eEvent.type == sf::Event::Closed)
	//		mWindow.close();

	//	if (eEvent.type == sf::Event::Resized) 
	//		printf("Resized");//just for resizing 

	//	if ((eEvent.type == sf::Event::KeyPressed) && (eEvent.key.code == sf::Keyboard::Escape))
	//		mWindow.close();
	//}
	mpManager->update(deltaT);

	//updates text
	mScoreText.setString("Score: " + std::to_string(mScoreNum));
	mLevelText.setString("Level: " + std::to_string(mLevelNum));
};
void RenderGame::Draw()
{
	mWindow.clear(sf::Color::Black);
	
	mpManager->draw(&mWindow);

	for (size_t i = 0; i < player1Lives.size(); i++)
	{
		mWindow.draw(player1Lives[i]);
	}
	for (size_t j = 0; j < player2Lives.size(); j++)
	{
		mWindow.draw(player2Lives[j]);
	}

	mWindow.draw(mP1Lives);
	mWindow.draw(mP2Lives);
	mWindow.draw(mScoreText);
	mWindow.draw(mLevelText);

	mWindow.display();
};

sf::Vector2i RenderGame::GetMousePosition()
{
	return sf::Mouse::getPosition(mWindow);
}

void RenderGame::Player1Lives(int lives)
{
	float xPos;
	float xScale = 1.1*LIVES_SCALE*mShipTexture.getSize().x;
	float yPos;
	float yScale = 1.01*(LIVES_SCALE * mShipTexture.getSize().y);

	player1Lives.clear();
	for (int i = 0; i < lives; i++)//P1
	{
		sf::Sprite sprite;
		sprite.setTexture(mShipTexture);
		sprite.scale(sf::Vector2f(LIVES_SCALE, LIVES_SCALE));
		sprite.setColor(sf::Color::White);
		xPos = (float)5 + (i * xScale);
		yPos = (float)WINDOW_HEIGHT - 5 - yScale;
		sprite.setPosition(xPos, yPos);
		//printf("%f\n", sprite.getPosition().x);
		player1Lives.push_back(sprite);
	}
};
void RenderGame::Player2Lives(int lives)
{
	float xPos;
	float xScale = 1.1*LIVES_SCALE*mShipTexture.getSize().x;
	float yPos;
	float yScale = 1.01*(LIVES_SCALE * mShipTexture.getSize().y);

	player2Lives.clear();
	for (int j = 0; j < lives; j++)//P2
	{
		sf::Sprite sprite;
		sprite.setTexture(mShipTexture);
		sprite.scale(sf::Vector2f(0.05f, 0.05f));
		sprite.setColor(sf::Color::White);
		xPos = (float)WINDOW_WIDTH - 20 - (j * xScale);
		yPos = (float)WINDOW_HEIGHT - 5 - yScale;
		sprite.setPosition(xPos, yPos);
		//printf("%f\n", sprite.getPosition().x);
		player2Lives.push_back(sprite);
	}
};