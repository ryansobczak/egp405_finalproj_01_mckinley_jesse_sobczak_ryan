#include "Astroid.h"
#include "PhysicsObjectManager.h"
#include "RandomData.h"

sf::Texture* Astroid::spSpriteSheet = nullptr;
PhysicsObjectManager* Astroid::spManager = nullptr;
int Astroid::sCount = 0;
float ASTROID_LEVEL_SCALE_MULTIPLE = 1;
int POINTS_TOTAL = 0;


Astroid::Astroid() : PhysicsObject()
{
	collision = false;
};
Astroid::~Astroid(){};

void Astroid::Create(int layer, float xPos, float yPos)
{
	CreateSub(layer);

	mTranform.Pos = sf::Vector2f(xPos, yPos);
};
void Astroid::CreateBase(int layer)
{
	CreateSub(layer);
};

void Astroid::Setup(int layer)
{
	mTranform.Self = sCount;
	sCount++;

	collision = false;
	mLayer = layer;
	mpSprite = new sf::Sprite;
	mpSprite->setTexture(*spSpriteSheet);

	int rand = gpRandom->GetNext() % (TEXTURE_ROWS * TEXTURE_COLS);

	int height = (TEXTURE_HEIGHT / TEXTURE_ROWS);
	int width = (TEXTURE_WIDTH / TEXTURE_COLS);
	sf::IntRect rect;
	rect.height = height;
	rect.width = width;
	mTranform.SpriteSheet = rand;

	int row = rand % TEXTURE_ROWS;
	int col = rand / TEXTURE_COLS;

	float Scale = ((float)layer)*BASE_SCALE*ASTROID_LEVEL_SCALE_MULTIPLE;
	mpSprite->setScale(Scale, Scale);
	mRadius = ((Scale*height) + (Scale*width)) / 4;

	rect.left = width * col;
	rect.top = height * row;
	mpSprite->setTextureRect(rect);
	mpSprite->setOrigin(height / 2, width / 2);

	switch (layer)
	{
	case 1:
		mpSprite->setColor(sf::Color::Green);
		break;
	case 2:
		mpSprite->setColor(sf::Color::Yellow);
		break;
	case 3:
		mpSprite->setColor(sf::Color::Red);
		break;
	default:
		break;
	}
};

void Astroid::CreateSub(int layer)
{
	mTranform.Self = sCount;
	sCount++;

	collision = false;
	mLayer = layer;
	mpSprite = new sf::Sprite;
	mpSprite->setTexture(*spSpriteSheet);

	int rand = gpRandom->GetNext() % (TEXTURE_ROWS * TEXTURE_COLS);

	int row = rand % TEXTURE_ROWS;
	int col = rand / TEXTURE_COLS;
	mTranform.SpriteSheet = rand;

	int height = (TEXTURE_HEIGHT / TEXTURE_ROWS);
	int width = (TEXTURE_WIDTH / TEXTURE_COLS);

	sf::IntRect rect;
	rect.height = height;
	rect.width = width;

	rect.left = width * col;
	rect.top = height * row;
	mpSprite->setTextureRect(rect);
	mpSprite->setOrigin(height / 2, width / 2);

	rand = gpRandom->GetNext();

	double RotationVel = ((rand % ROTATION_VARIANCE) + ROTATION_MIN)*ROTATION_REDUCTION;
	if ((rand % (ROTATION_VARIANCE * 2)) > ROTATION_VARIANCE)
		RotationVel = -RotationVel;

	rand = gpRandom->GetNext();
	double Rotation = ((rand % 3600) / 10);
	double VecAngle = ((rand % 360)*PI / 180);
	mpSprite->setRotation(Rotation);

	mTranform.CurRotation = Rotation;
	mTranform.RVel = RotationVel;

	float Scale = ((float)layer)*BASE_SCALE*ASTROID_LEVEL_SCALE_MULTIPLE;
	mpSprite->setScale(Scale, Scale);
	mRadius = ((Scale*height) + (Scale*width)) / 4;

	rand = gpRandom->GetNext();
	float Vel = rand % VAR_VEL + MIN_VEL;
	Vel = Vel*RED_VEL;

	rand = gpRandom->GetNext() % 360;
	rand = rand * PI / 180;
	mTranform.Vel = sf::Vector2f(Vel*cos(VecAngle), Vel*sin(VecAngle));

	switch (layer)
	{
	case 1:
		mpSprite->setColor(sf::Color::Green);
		break;
	case 2:
		mpSprite->setColor(sf::Color::Yellow);
		break;
	case 3:
		mpSprite->setColor(sf::Color::Red);
		break;
	default:
		break;
	}
};

bool Astroid::update(double deltaTime)
{
	if (collision)
	{
		POINTS_TOTAL++;
		if (mLayer > 1)
		{
			//create more astroids.
			Astroid* next1 = new Astroid();
			Astroid* next2 = new Astroid();
			next1->Create(mLayer - 1, mTranform.Pos.x, mTranform.Pos.y);
			next2->Create(mLayer - 1, mTranform.Pos.x, mTranform.Pos.y);

			next1->mTranform.Parent = mTranform.Self;
			next2->mTranform.Parent = mTranform.Self;
			
			spManager->AddToLayer(mLayer - 1, next1);
			spManager->AddToLayer(mLayer - 1, next2);
		}
		return true;
	}
	else
	{
		mpSprite->rotate(mTranform.RVel* deltaTime);

		sf::Vector2f pos = mTranform.Pos += (mTranform.Vel * (float)deltaTime);
		mpSprite->setPosition(pos);

		BoarderCheck();
		return false;
	}
};

bool Astroid::CheckCollision(PhysicsObject* obj)
{
	sf::Vector2f diff = obj->GetTransform().Pos - mTranform.Pos;
	float distance = sqrt(diff.x*diff.x + diff.y*diff.y);
	if (distance < mRadius)
	{
		collision = true;
		return true;
	}
	return false;
};
void Astroid::Collision(PhysicsObject* obj)
{
	printf("Collision\n");
	collision = true;
};

void Astroid::draw(sf::RenderWindow* window)
{
	window->draw(*mpSprite);
};

void Astroid::SetSpriteSheet(sf::Texture* SpriteSheet)
{
	spSpriteSheet = SpriteSheet;
};
void Astroid::SetManager(PhysicsObjectManager* manager)
{
	spManager = manager;
};


void Astroid::SetTransform(Transform trans)
{
	mTranform = trans;

	int height = (TEXTURE_HEIGHT / TEXTURE_ROWS);
	int width = (TEXTURE_WIDTH / TEXTURE_COLS);
	sf::IntRect rect;
	rect.height = height;
	rect.width = width;

	int row = mTranform.SpriteSheet % TEXTURE_ROWS;
	int col = mTranform.SpriteSheet / TEXTURE_COLS;
	rect.left = width * col;
	rect.top = height * row;
	mpSprite->setTextureRect(rect);
};