#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"
#include "RandomData.h"

#define PI 3.14159265
const int SPARE_ASTROID = 1;

//class PhysicsObject;
#pragma pack(push, 1)
struct Transform
{
	//current screen position;
	sf::Vector2f Pos;
	//current rotation value;
	double CurRotation;

	//current screen vel
	sf::Vector2f Vel;
	//current rotational vel
	double RVel;

	int Parent;
	int Self;
	int SpriteSheet;
};
  #pragma pop
namespace messages {
	enum value_type : unsigned char
	{
		ID_TTT_TURN_AND_BOARD = ID_USER_PACKET_ENUM,
		ID_TTT_OTHER_USER_WON,
		ID_TTT_INIT_PLAYER,//give me my number!
		ID_TTT_PLAYER_NUM,//receiving number!
		ID_TTT_GAME_START,//all players registered.
		ID_TTT_PLAYER_UPDATE,//update from a single player
		ID_TTT_STATE_BROADCAST,//update the game state.
		ID_TTT_PLAYER_MOVED,//send the movement to the server
		ID_TTT_CHAT,//just a chat message
		ID_TTT_OTHER_PLAYER_LOST_CONNECTION,
		ID_TTT_PLAYER_SHOT,
		ID_TTT_LASER_MOVE,
		ID_TTT_ASTEROID_EXPLODE,
		ID_TTT_ASTEROID_MOVE,
		ID_TTT_CHANGE_LEVEL
	};
};
/*
A NOTE ON DATA:
The destination position is a limiting factor on the objects velocity, not where the object should be.
For the players, this is where there mouse is located, to stop the paddles from exeding a certain place
For the Ball, this is a nonsensical value, impossible to reach.
*/
//player data.
#pragma pack(push, 1)
struct PlayerData
{
	unsigned char packetID;
	RakNet::Time timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
	int playerNum = 0;
	int level;
	Transform playerTrans;
};
#pragma pop
#pragma pack(push, 1)
struct ServerData
{
	unsigned char packetID;
	//unsigned char useTimeStamp; // Assign ID_TIMESTAMP to this
	double timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
	
	Transform Player1;
	Transform Player2;


	int HighMed;
	int MedLow;
	int low;
	Transform Astroids[46];//(((x+1)*2)+1 *2); x == 10

	int Num1;
	int Num2;
	Transform Laser1[50];
	Transform Laser2[50];
	RandomData RandomNum;

	int score;
	int p1Lives;
	int p2Lives;
	int level;
};
#pragma pop
#pragma pack(push, 1)
struct AsteroidData
{
	unsigned char packetID; // Assign ID_TIMESTAMP to this
	unsigned char useTimeStamp;
	double timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
	Transform Astroids[46];//(((x+1)*2)+1 *2); x == 10
};
#pragma pop
#pragma pack(push, 1)
struct ClientData
{
	unsigned char packetID;
	//unsigned char useTimeStamp; // Assign ID_TIMESTAMP to this
	double timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value

	int PlayerNum;
	Transform Player;
	int num;
	Transform Laser[20];

	int level;
};
#pragma pop
//player data.
//#pragma pack(push, 1)
//struct LaserData
//{
//	unsigned char packetID;
//	unsigned char useTimeStamp; // Assign ID_TIMESTAMP to this
//	RakNet::Time timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
//	std::vector<PlayerData*> Lazers;
//};
//#pragma pop
//game state to force players.
#pragma pack(push, 1)
struct GameState
{
	unsigned char packetID;
	unsigned char useTimeStamp; // Assign ID_TIMESTAMP to this
	RakNet::Time timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
	PlayerData Player1;
	PlayerData Player2;
	PlayerData Lazer;

	int score;
	int p1Lives;
	int p2Lives;
	int level;
};
#pragma pop
#pragma pack(push, 1)
struct GameStart
{
	unsigned char packetID;
	RandomData RandomNum;
	RakNet::Time timeStamp; // Put the system time in here returned by RakNet::GetTime() or some other method that returns a similar value
};
#pragma pop
#pragma pack(push, 1)
struct playerNumber
{
	unsigned char packetID;
	int playerNum = 0;
};
#pragma pop
#pragma pack(push, 1)
struct chatMessage
{
	unsigned char packetID;
	char message[2028];
};
#pragma pop

const int OFFSET = 10;
const int MAX_TIME = 0.12;
const float TIME_STEP = 0.1;
const float TIME_ROT_STEP = 0.55;
const int MAX_BUFFER_SIZE = 20;
const float ROT_VALUE = 10.0;
const float LAZER_VELOCITY = 7.5;
const float VELOCITY = 5.0;
const float ACCELERATION = 2.0;
const int UPDATE_TIME = 10; 
const int RAKNET_UPDATE_VALUE = 20;
const int WINDOW_HEIGHT = 600;
const int WINDOW_WIDTH = 600;
const int STARTING_LIVES = 5;