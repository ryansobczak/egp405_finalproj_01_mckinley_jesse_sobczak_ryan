#include "PlayerShip.h"
#include <math.h>

PlayerShip::PlayerShip()
{
	curVel = 0;
	mTranform.Vel = sf::Vector2f(0,0);
	invincible = false;
	live = true;
};
PlayerShip::~PlayerShip(){};

bool PlayerShip::update(double deltaTime)
{
	if (invincible)
	{
		invTime -= deltaTime;
		if (invTime < 0)
		{
			invincible = false;
			sf::Vector2f scale = mShape->getScale();
			scale.x *= 2;
			scale.y *= 2;
			mShape->setScale(scale);
		}
	}
	if (mTranform.RVel != 0)
	{
		mTranform.CurRotation += mTranform.RVel*deltaTime;
		if (mTranform.CurRotation < 0)
		{
			mTranform.CurRotation += 360;
		}
		else if (mTranform.CurRotation > 360)
		{
			mTranform.CurRotation -= 360;
		}
		mTranform.Vel.x = sin(mTranform.CurRotation * PI / 180.0)*curVel;
		mTranform.Vel.y = -cos(mTranform.CurRotation * PI / 180.0)*curVel;
		mShape->setRotation(mTranform.CurRotation);
	}
	mTranform.Pos.x += mTranform.Vel.x*deltaTime;
	mTranform.Pos.y += mTranform.Vel.y*deltaTime;

	BoarderCheck();
	mShape->setPosition(mTranform.Pos);
	return false;
};
/*void PlayerShip::draw(sf::RenderWindow* window)
{

};*/
void PlayerShip::SetTransform(Transform trans)
{
	mPrevTransform = mTranform;
	mTranform = trans;
	curVel = sqrt(trans.Vel.x*trans.Vel.x + trans.Vel.y*trans.Vel.y);
	if (mShape != nullptr)
	{
		mShape->setRotation(mTranform.CurRotation);
	}
};

bool PlayerShip::CheckCollision(PhysicsObject* obj)
{
	if (invincible)
		return false;
	return PhysicsObject::CheckCollision(obj);
};
void PlayerShip::Collision(PhysicsObject* obj)
{
	if (!invincible)
	{
		sf::Vector2f scale = mShape->getScale();
		scale.x /= 2;
		scale.y /= 2;
		mShape->setScale(scale);
	}
	invTime = INV_TIME;
	invincible = true;
	live = false;

};
void PlayerShip::Accelerate(float inc)
{
	if (curVel < MAX_SPEED)
	{
		curVel += inc;
		if (curVel > MAX_SPEED)
		{
			curVel = MAX_SPEED;
		}
		
		mTranform.Vel.x = sin(mTranform.CurRotation * PI / 180.0)*curVel;
		mTranform.Vel.y = -cos(mTranform.CurRotation * PI / 180.0)*curVel;

	}
};
void PlayerShip::Deccelerate(float inc)
{

	if (curVel > 0)
	{
		curVel -= inc;
		if (curVel < 0)
		{
			curVel = 0;
		}

		mTranform.Vel.x = sin(mTranform.CurRotation * PI / 180.0)*curVel;
		mTranform.Vel.y = -cos(mTranform.CurRotation * PI / 180.0)*curVel;

	}
};


bool PlayerShip::isLive()
{
	bool temp = live;
	live = true;
	return temp;
};


void PlayerShip::SetPlayer(int PlayerNum)
{
	if (PlayerNum == 1)
	{
		mShape->setColor(sf::Color::Blue);
	}
	else if (PlayerNum == 2)
	{
		mShape->setColor(sf::Color(255,0,255,255));
	}
};