#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"
#include "..\render\renderGame.h"

#include "..\render\CommonData.h"
#include "..\render\AsteroidGame.h"


class Server
{
public:
	Server();
	~Server();
	int main();
	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	bool LaunchServer(RakNet::RakPeerInterface* a_peer, int a_port, bool isCommandLine);
	bool HandlePackets(RakNet::RakPeerInterface* a_peer);

	//implement later
	void Update(double deltaTime, RakNet::RakPeerInterface* a_peer);

private:
	sf::Vector2f checkBorders(sf::Vector2f pos);
	long int absoluteValue(time_t value);
	std::vector<int> PriorStateBlocks;
	//may not need, may replace with physics object manager.

	AstroidGame* mpGame;
	//PhysicsObject* mBall

	chatMessage mChat;
	GameState mGameState;

	int currentPlayers = 0;
	long int mStartTime;
	long int mCurrentTime;
	long int mScore = 0;
	int mPlayer1Lives;
	int mPlayer2Lives;
	std::vector<RakNet::Packet*> mPacketBuffer;
	int mCounter = 0;
	bool mStartedGame = false;
	bool mIsCommandLine = false;


	double mTimer;
};