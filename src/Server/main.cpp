#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Server.h"

int main(int argc, char* argv[])
{
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	Server* pServer = new Server();

	if (argc > 1)
	{
		if (argc < 2)
			return 0;
		else
		{
			if (pServer->LaunchServer(peer, std::stoi(argv[2 - 1]), true) == false)
				return 0;
		}
	}
	else
	{
		if (pServer->LaunchServer(peer, 200, false) == false)
			return 0;
	}

	LARGE_INTEGER frequency;
	LARGE_INTEGER t1, t2;
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);
	while (pServer->HandlePackets(peer))
	{
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		QueryPerformanceCounter(&t1);
		pServer->Update(elapsedTime, peer);

#ifdef _WIN32
		Sleep(RAKNET_UPDATE_VALUE);
#else 
		usleep(RAKNET_UPDATE_VALUE * 1000);
#endif
	}

	system("Pause");
	peer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(peer);
	
	return 0;
}
