#include "server.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Gets.h"
#include <fstream>
#include <cmath>

Server::Server()
{
	mCurrentTime = 0;
	mPlayer1Lives = STARTING_LIVES;
	mPlayer2Lives = STARTING_LIVES;

	mpGame = nullptr;
	mTimer = 0;
}

Server::~Server()
{
	if (mpGame != nullptr){
		delete mpGame;
		mpGame = nullptr;
	}
}


unsigned char Server::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

bool Server::LaunchServer(RakNet::RakPeerInterface* a_server, int a_port, bool isCommandLine)
{
	mIsCommandLine = isCommandLine;

	mCurrentTime = 0;
	std::ofstream file("IPAddress.txt");
	a_server->SetIncomingPassword(0, 0);

	// IPV4 socket
	RakNet::SocketDescriptor  sd;
	sd.port = a_port;
	sd.socketFamily = AF_INET;
	printf("Server port: %d\n", a_port);

	if (a_server->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
	{
		printf("\nFailed to start server with IPV4 ports");
		return false;
	}

	a_server->SetOccasionalPing(true);
	a_server->SetUnreliableTimeout(1000);
	a_server->SetTimeoutTime(4000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	a_server->SetMaximumIncomingConnections(4);

	printf("\nSERVER IP addresses:");
	for (unsigned int i = 0; i < a_server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = a_server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("\n%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
	file << a_server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, 0).ToString();
	file.close();

	return true;
}

bool Server::HandlePackets(RakNet::RakPeerInterface* a_peer)
{
	for (RakNet::Packet* p = a_peer->Receive(); p; a_peer->DeallocatePacket(p), p = a_peer->Receive())
	{
		auto packetID = GetPacketIdentifier(p);

		//packet buffer
		if (mPacketBuffer.size() > MAX_BUFFER_SIZE)
		{
			assert(!mPacketBuffer.empty());
			mPacketBuffer.erase(mPacketBuffer.begin());
		}
		mPacketBuffer.push_back(p);

		switch (packetID)
		{
#pragma region Base_Trans
			case ID_DISCONNECTION_NOTIFICATION:
			{
				printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));;
				break;
			}
			case ID_CONNECTION_LOST:
			{ 
				printf("\nConnection lost from %s", p->systemAddress.ToString(true));

				currentPlayers--;
				if (currentPlayers < 0) 
					currentPlayers = 0;

				mStartedGame = false;
				unsigned char packetID = (unsigned char)messages::ID_TTT_OTHER_PLAYER_LOST_CONNECTION;
				a_peer->Send((const char*)&(packetID), sizeof(packetID), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				break; 
			}
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("\nNew incoming connection from %s with GUID %s",
					p->systemAddress.ToString(true), p->guid.ToString());
				break;
			}
#pragma endregion
#pragma region Init_Game_Trans
			case messages::ID_TTT_INIT_PLAYER:
			{
				printf("Request from %s for player number", p->systemAddress.ToString(true));
				if (currentPlayers < 2)
				{
					playerNumber newPlayer;
					newPlayer.packetID = messages::ID_TTT_PLAYER_NUM;
					newPlayer.playerNum = ++currentPlayers;
					a_peer->Send((const char*)&(newPlayer), sizeof(playerNumber), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, false);

					if (currentPlayers >= 2)
					{
						gpRandom = new RandomDataStorage;
						gpRandom->Setup();

						mpGame = new AstroidGame(mIsCommandLine);
						mpGame->CreateBoard();
						
						mStartedGame = true;

						time_t  timev;
						GameStart start;
						start.RandomNum = gpRandom->SendData();
						//start.useTimeStamp = ID_TIMESTAMP;
						mStartTime = start.timeStamp = absoluteValue(time(&timev));
						start.packetID = messages::ID_TTT_GAME_START;
						//mCurrentTime = start.CurrentTime = mStartTime - mCurrentTime;
						printf("\nstart time %ld\n", mStartTime);
						a_peer->Send((const char*)&(start), sizeof(start), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
					}
				}
				break;
			}
#pragma endregion
			case messages::ID_TTT_PLAYER_UPDATE:
			{
				ClientData* data = (ClientData*)p->data;
				assert(p->length == sizeof(ClientData));
				if (p->length != sizeof(ClientData))
					return false;

				mpGame->SetClientState(*data);
				break;
			}
			case messages::ID_TTT_CHAT:
			{
				chatMessage *chat = (chatMessage*)p->data;
				mChat = *chat;
				//printf("%s\n", chat->message);
				mChat.packetID = (unsigned char)messages::ID_TTT_CHAT;
				a_peer->Send((const char*)&(mChat), sizeof(mChat), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, true);
				break;
			}

			default:
			{
				printf("%s\n", p->data);
				break;
			}
		}
	}

	return true;
}

sf::Vector2f Server::checkBorders(sf::Vector2f pos)
{
	sf::Vector2f newPos = pos;

	if (newPos.x > WINDOW_WIDTH - OFFSET)
		newPos.x = WINDOW_WIDTH - OFFSET;
	if (newPos.x < OFFSET)
		newPos.x = OFFSET;

	if (newPos.y > WINDOW_HEIGHT - OFFSET)
		newPos.y = WINDOW_HEIGHT - OFFSET;
	if (newPos.y < OFFSET)
		newPos.y = OFFSET;

	return newPos;
}

void Server::Update(double deltaTime, RakNet::RakPeerInterface* a_peer)
{
	if (currentPlayers >= 2)
	{

		mpGame->Update(deltaTime);
		mpGame->Draw();
		mCurrentTime += deltaTime;
		mTimer += deltaTime;
		if (mTimer >= 1000 / 30)
		{
			mTimer = 0;
			ServerData data = mpGame->GetServerState();
			a_peer->Send((const char*)&(data), sizeof(data), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

		}
	}
};

long int Server::absoluteValue(time_t value)
{
	long int newValue = static_cast<long int>(value);
	if (newValue < 0)
	{
		printf("Negitive value\n");
		newValue += 2 * newValue;
	}
	return value;
};