#include "client.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Gets.h"
#include "Kbhit.h"
#include "SFML/Window/Mouse.hpp"
#include <iostream>
#include <fstream>

#ifdef _WIN32 
#include "WindowsIncludes.h"
#else 
#include <unistd.h>
#endif



int main(int argc, char* argv[])
{
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	Client* pClient = new Client();
	//const std::string CONST_IP = "192.168.18.1";//laptop
	const std::string CONST_IP = "184.171.153.176";//desktop

	if (argc > 1)
	{
		if (argc == 4)
		{
			//clientport, server address, serverport
			if (pClient->LaunchClient(peer, std::stoi(argv[1]), argv[2], std::stoi(argv[3]), true) == false)
				return 0;
		}
		else
			return 0;
	}
	else
	{
		std::ifstream file("IPAddress.txt");
		//gets the port 
		int port;
		std::cout << "Enter in Client port: ";
		std::cin >> port;
		std::string IP;
		file >> IP;
		//if (pClient->LaunchClient(peer, port, IP.c_str() , 200) == false)/
		srand(time(NULL));
		//int port1 = rand() % 200 + 50;
		if (pClient->LaunchClient(peer, port, IP.c_str(), 200, false) == false)
			return 0;
	}
	LARGE_INTEGER frequency;
	LARGE_INTEGER t1, t2;
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);
	while (pClient->HandlePackets(peer))
	{

		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		QueryPerformanceCounter(&t1);
		pClient->Update(elapsedTime, peer);

#ifdef _WIN32
		Sleep(RAKNET_UPDATE_VALUE);
#else 
		usleep(RAKNET_UPDATE_VALUE * 1000);
#endif
	}

	system("Pause");
	peer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(peer);

	return 0;
}