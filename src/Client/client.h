#pragma once
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "RakPeer.h"
#include "..\render\renderGame.h"
#include "..\render\CommonData.h"
#include <SFML\Window\Event.hpp>

#include "..\Render\AsteroidGame.h"

class Client
{
	CONST float PAD_SPEED = 0.2f;
public:

	Client();
	~Client();
	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	bool LaunchClient(RakNet::RakPeerInterface* a_peer, int a_clientPort, const char* a_serverIP, int a_serverPort, bool isCommandLine);
	bool HandlePackets(RakNet::RakPeerInterface* a_peer);

	void Update(double deltaTime, RakNet::RakPeerInterface* a_peer);


private:
	sf::Vector2f checkBorders(sf::Vector2f pos);
	void cleanup();
	void HardUpdate(PhysicsObject* object, PlayerData* data);
	void HardUpdatePad(PhysicsObject* object, PlayerData* data);
	sf::Vector2i mPriorMouse;

	AstroidGame* mpGame;

	//PhysicsObject* mBall;

	//players mPlayer
	PlayerData data;
	std::vector<PhysicsObject*> lazers;
	chatMessage mChat;

	bool mIsActive;
	//int mPlayers;
	int mPlayerID;
	bool mIsCommandLine = false;
	bool justShot = false;
	int mPlayerLives;
	long int mStartTime;
	long int CurrentTime;
	std::vector<RakNet::Packet*> mPacketBuffer;

	double mTimer;
	bool init;
};