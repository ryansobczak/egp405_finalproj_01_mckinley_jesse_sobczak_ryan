#include "client.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "Gets.h"
#include "Kbhit.h"

#include <iostream>

Client::Client()
{
	mTimer = 0;
	mIsActive = false;
	CurrentTime = 0;
	mPlayerLives = STARTING_LIVES;
	mpGame = nullptr;
}

Client::~Client()
{
	cleanup();
}

void Client::cleanup()
{
	//mBall = NULL;
	if (mpGame != nullptr){
		delete mpGame;
		mpGame = nullptr;
	}
}

unsigned char Client::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

bool Client::LaunchClient(RakNet::RakPeerInterface* a_client, int a_clientPort, const char* a_serverIP, int a_serverPort, bool isCommandLine)
{
	mIsCommandLine = isCommandLine;
	CurrentTime = 0;
	// IPV4 socket
	RakNet::SocketDescriptor sd(a_clientPort, 0);
	sd.socketFamily = AF_INET;

	int startupResult = a_client->Startup(1, &sd, 1);
	a_client->SetOccasionalPing(true);

	if (a_client->Connect(a_serverIP, a_serverPort, 0, 0) !=
		RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("Attempt to connect to server FAILED\n");
		return false;
	}

	printf("\nCLIENT IP addresses:\n");
	for (unsigned int i = 0; i < a_client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, a_client->GetLocalIP(i));
	}
	mpGame = new AstroidGame(mIsCommandLine);
	mpGame->CreateScreen();
	mpGame->CreateSandbox();

	return true;
}

bool Client::HandlePackets(RakNet::RakPeerInterface* a_peer)
{
	//if (_kbhit())
	//{
	//	//chat messages
	//	chatMessage chat2;
	//	Gets(mChat.message, sizeof(mChat.message));
	//	std::string prefix = "Player" + std::to_string(mPlayerID) + ": ";

	//	strncpy_s(chat2.message, prefix.c_str(), sizeof(chat2.message));
	//	strncat_s(chat2.message, mChat.message, sizeof(chat2.message) - strlen(prefix.c_str()) - 1);

	//	if (sizeof(mChat.message) > 1)
	//	{
	//		chat2.packetID = (unsigned char)messages::ID_TTT_CHAT;
	//		a_peer->Send((const char*)&(chat2), sizeof(chat2), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	//	}
	//}

	for (RakNet::Packet* p = a_peer->Receive();	p; a_peer->DeallocatePacket(p), p = a_peer->Receive())
	{
		auto packetID = GetPacketIdentifier(p);

		//packet buffer
		if (mPacketBuffer.size() > MAX_BUFFER_SIZE)
		{
			assert(!mPacketBuffer.empty());
			mPacketBuffer.erase(mPacketBuffer.begin());
		}
		mPacketBuffer.push_back(p);

		switch (packetID)
		{
#pragma region Base_Trans
			// Handle client packets
			case ID_CONNECTION_LOST:
			{
				printf("\nConnection lost from %s", p->systemAddress.ToString(true));
				cleanup();
				break;
			}
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			{ printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n"); break; }
			case ID_DISCONNECTION_NOTIFICATION:
			{ printf("ID_DISCONNECTION_NOTIFICATION\n"); break;}
			case ID_ALREADY_CONNECTED:
			{ printf("\nAlready connected with guid %s", p->guid); break; }
			case ID_CONNECTION_BANNED:
			{ printf("\nWe are banned from this server"); return false; }
			case ID_CONNECTION_ATTEMPT_FAILED:
			{ printf("\nConnection to server failed"); return true; }
			case ID_INVALID_PASSWORD:
			{ printf("\nPassword invalid"); return false; }
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Connection require ACCEPTED to %s with GUID %s\n",
					p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n",
					a_peer->GetExternalID(p->systemAddress).ToString(true));

				unsigned char packet = (unsigned char)messages::ID_TTT_INIT_PLAYER;
				a_peer->Send((const char*)&(packet), sizeof(packet), HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, false);
				break;
			}

			case messages::ID_TTT_OTHER_PLAYER_LOST_CONNECTION:
			{
				mIsActive = false;
				if (mPlayerID == 1)
				{
					printf("Player2 lost connection with the server.\n");
					mPlayerID = 1;
					printf("You are still Player %d. Waiting for Player 2.\n", mPlayerID);
				}
				else if (mPlayerID == 2)
				{
					printf("Player1 lost connection with the server.\n");
					mPlayerID = 1;
					printf("You are now Player %d. Waiting for Player 2.\n", mPlayerID);
				}

				mpGame->CreateSandbox();

				break;
			}

#pragma endregion

#pragma region Init_Game_Trans

			case messages::ID_TTT_PLAYER_NUM:
			{
				printf("Recieved player number");
				playerNumber *tmpPlayer = (playerNumber*)p->data;
				assert(p->length == sizeof(playerNumber));
				if (p->length != sizeof(playerNumber))
					return false;

				if (tmpPlayer->playerNum == 1)
				{
					//inits player data
					mPlayerID = 1;
					printf("\nWaiting for player 2.\n");
				}
				else if (tmpPlayer->playerNum == 2)
				{
					//inits player data
					mPlayerID = 2;
					printf("\nWaiting to start.\n");
				}
				break;
			}
			case messages::ID_TTT_GAME_START:
			{
				printf("Starting\n");
				mIsActive = true;
				GameStart *pStart = (GameStart*)p->data;
				assert(p->length == sizeof(GameStart));
				if (p->length != sizeof(GameStart))
					return false;
				//PhysicsObject* mBall = new PhysicsObject;

				if (mpGame == nullptr)
				{
					mpGame = new AstroidGame(mIsCommandLine);
					mpGame->CreateScreen();
				}

				printf("before random\n");
				if (gpRandom == nullptr)
					gpRandom = new RandomDataStorage;
				gpRandom->Save(pStart->RandomNum);
				printf("after random\n");

				mpGame->CreateBoard();
				mpGame->SetPlayer(mPlayerID);
				//mpEvent = new sf::Event();


				mStartTime = pStart->timeStamp;
				printf("\n startTime: %ld", mStartTime);
				CurrentTime = 0;
				mpGame->SetTime(CurrentTime);

				init = true; 
				break;
			}
#pragma endregion

			case messages::ID_TTT_STATE_BROADCAST:
			{
				ServerData* data = (ServerData*)p->data;
				assert(p->length == sizeof(ServerData));
				if (p->length != sizeof(ServerData))
					return false;
				if (init)
				{
					init = false;
					mpGame->SetTime(data->timeStamp - 20.0f);
				}
				mpGame->SetServerState(*data);
				//CurrentTime -= data->timeStamp;
				//get score
				//get lives
				//get level

				break;
			}

			case messages::ID_TTT_CHAT:
			{
				chatMessage *chat = (chatMessage*)p->data;
				printf("%s\n", chat->message);
				break;
			}

			default:
			{
				printf("\nReceived unhandled packet %c", GetPacketIdentifier(p));
				break;
			}
		}
	}
	return true;
}
void Client::Update(double deltaTime, RakNet::RakPeerInterface* a_peer)
{
	/*while (mpGame->InputStackCheck())
	{
		PlayerData data = mpGame->GetNextInput();
		a_peer->Send((const char*)&(data), sizeof(data), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}*/
	if (mpGame != nullptr)
	{
		mpGame->Update(deltaTime);
		mpGame->Draw();
		if (mIsActive){
			mTimer += deltaTime;
			if (mTimer >= 1000 / 30)
			{
				mTimer = 0;
				ClientData data = mpGame->GetClientState();
				a_peer->Send((const char*)&(data), sizeof(data), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			}
		
		}
	}
}
void Client::HardUpdate(PhysicsObject* object, PlayerData* data)
{};
void Client::HardUpdatePad(PhysicsObject* object, PlayerData* data)
{};

sf::Vector2f Client::checkBorders(sf::Vector2f pos)
{
	sf::Vector2f newPos = pos;

	if (newPos.x > WINDOW_WIDTH - OFFSET)
		newPos.x = WINDOW_WIDTH - OFFSET;
	if (newPos.x < OFFSET)
		newPos.x = OFFSET;

	if (newPos.y > WINDOW_HEIGHT - OFFSET)
		newPos.y = WINDOW_HEIGHT - OFFSET;
	if (newPos.y < OFFSET)
		newPos.y = OFFSET;

	return newPos;
}