Team:
Jesse McKinley
Ryan Sobczak

URL to Presentation:
https://docs.google.com/presentation/d/1kWGmzDQEyXuszTlHdojErkOZOhF4NBUbz0ClKPyBxZQ/edit?usp=sharing

URL to the Repo:
https://bitbucket.org/ryansobczak/egp405_finalproj_01_mckinley_jesse_sobczak_ryan

-IMPORTANT-
Starting the Game:
-Build the Server first, or the whole solution, to create the dll files into the same folder as the .exe programs

To connect the client to the server type in:
"Client.exe ClientPort ServerIP ServerPort"
To init the server type in:
"Server.exe ServerPort"

Game Description:
This is a asteriods game that uses networking and 2D graphics.
The networking is done using RakNet
The 2D graphics is done using SFML

Controls:
-Game Controls-
WASD: Move the Ship (AD) steers, W to go forward, S to stop.
Space: Shoot

Unique Features:
-New-
Improved Physics system
Singleplayer along with multiplayer

-From Past Assignment, still in-
Robust physics system 
Can connect a player to another one, when one player disconnects